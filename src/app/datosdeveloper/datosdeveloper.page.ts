import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-datosdeveloper',
  templateUrl: './datosdeveloper.page.html',
  styleUrls: ['./datosdeveloper.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class DatosdeveloperPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DatosdeveloperPage } from './datosdeveloper.page';

describe('DatosdeveloperPage', () => {
  let component: DatosdeveloperPage;
  let fixture: ComponentFixture<DatosdeveloperPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DatosdeveloperPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
